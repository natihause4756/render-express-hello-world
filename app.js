const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const port = process.env.PORT || 3001;

app.use(bodyParser.text({ type: "*/*" }));
app.use(cors());

app.get("/", handler);
app.post("/", handler);

function handler(req, res) {
  const logMsg = `${req.ip} ${req.originalUrl} ${req.body}`;
  console.log(logMsg);
  res.type("html").send("");
}

const server = app.listen(port, () =>
  console.log(`Example app listening on port ${port}!`)
);

server.keepAliveTimeout = 120 * 1000;
server.headersTimeout = 120 * 1000;
